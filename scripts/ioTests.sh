#!/bin/awk -f


function red(s) {
    printf "\033[1;31m" s "\033[0m"
}

function green(s) {
    printf "\033[1;32m" s "\033[0m"
}

function blue(s) {
    printf "\033[1;34m" s "\033[0m"
}

function INFO(){
   printf("["); blue("INFO"); printf("]")
}

BEGIN { 
   print("\n")
   INFO()
   print(" -------------------------------------------------------")
   INFO()
   print(" I / O   T E S T S")
   INFO()
   print(" -------------------------------------------------------")

   INFO()
   print (" I/O tests from " ARGV[1] ":"); 
   RS = "----\n"
   n_tests = 0
   errors = 0
}


match($0, /Esperado:(.*)/, a)   {esperado = a[1]; Esperado=$0; n_tests += 1; next} 

match($0, /Encontrado:(.*)/, a) {encontrado = a[1]; Encontrado = $0} 

{
    if (esperado != encontrado) 
    {
        print("\n")
        red(Esperado)
        print "----"
        red(Encontrado)
        print "----"; 
        errors += 1
    }
}

END{
    if (errors > 0) {
        INFO()
        red(" Tests run:"); red(n_tests); red(", Errors: "); red(errors); red("\n\n") 
        exit 1
    }
    else {
        INFO()
        green(" Tests run:"); green(n_tests); green(", Errors: "); green(errors); print("\n") 
    }
}
